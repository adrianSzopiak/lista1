import Main.days

object zad4 {

  def runZad(): Unit = {
    println("zad4")
    println(foldLeft(days))
    println(foldRight())
    println(foldLeftWithM())
  }

  def foldLeft(list: List[String]): String = list.tail.foldLeft(list.head)(_ + "," + _)

  def foldRight(): String = days.dropRight(1).foldRight(days.last)(_ + "," + _)

  def foldLeftWithM(): String = foldLeft(days.filter(p => p.startsWith("M")))
}
