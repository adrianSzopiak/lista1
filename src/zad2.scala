import Main.days
object zad2 {

  def runZad() {
    println("zad2")
    println(recurrence())
    println(recurrenceFromEnd())
  }

  def recurrence(): String = {
    iter(days)
  }

  def recurrenceFromEnd(): String = {
    iter(days.reverse)
  }

  def iter(list: List[String]): String = {
    if(list.isEmpty) ""
    else if (list.size == 1) list.head
    else list.head + "," + iter(list.tail)
  }
}
