import zad5.itemMap

object zad7 {

  def runZad(): Unit = {
    println("zad7")
    println(getWithOption("phone"))
  }

  def getWithOption(name: String): Option[Double] = itemMap.get(name)
}
