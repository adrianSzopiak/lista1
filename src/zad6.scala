object zad6 {

  def runZad(): Unit = {
    println("zad6")
    printTuple("bleh", 23.2, false)
  }

  def printTuple(tuple: (Any, Any, Any)): Unit = {
    println(tuple._1.toString + " , " + tuple._2.toString + " , " + tuple._3.toString)
  }
}
