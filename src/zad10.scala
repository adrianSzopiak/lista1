import scala.math.abs

object zad10 {

  val originalList = List(-10, -9, -5, -4, -2, -1, 0, 2, 7, 12, 80, 123)

  def runZad(): Unit = {
    println("zad10")
    println(absListFromMinus5To12(originalList))
  }

  def absListFromMinus5To12(list: List[Int]): List[Int] = list.filter(value => value > -6 && value < 13).map(value => abs(value))
}
