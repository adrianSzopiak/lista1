import Main.days

object zad3 {

  def runZad() {
    println("zad3")
    println(iter(days.tail, days.head))
  }

  @annotation.tailrec
  def iter(list: List[String], result: String): String = {
    val nextValue = list.head
    val nextResult = result.concat("," + nextValue)
    if (nextValue == list.last) return nextResult
    else iter(list.tail, nextResult)
  }
}
