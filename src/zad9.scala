import zad8.listWithZeros

object zad9 {

  def runZad(): Unit = {
    println("zad9")
    println(returnPlus1List(listWithZeros))
  }

  def returnPlus1List(list: List[Int]): List[Int] = list.map(value => value + 1)
}
