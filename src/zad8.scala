object zad8 {

  val listWithZeros = List(0,8,1,3,5,67,8,8,0,1235,3,56,6,0,0,0,1)

  def runZad(): Unit = {
    println("zad8")
    println(deleteZeroRec())
  }

  def deleteZeroRec(): List[Int] = {
    @annotation.tailrec
    def iter(list: List[Int], result: List[Int]): List[Int] = {
      if(list.isEmpty) result
      else if(list.head == 0) iter(list.tail, result)
      else iter(list.tail, result.appended(list.head))
    }
    iter(listWithZeros, List())
  }
}
