object Main {

  val days = List("Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday", "Sunday")

  def main(args: Array[String]): Unit = {
    zad1.runZad()
    zad2.runZad()
    zad3.runZad()
    zad4.runZad()
    zad5.runZad()
    zad6.runZad()
    zad7.runZad()
    zad8.runZad()
    zad9.runZad()
    zad10.runZad()
  }
}
