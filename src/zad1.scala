import Main.days
object zad1 {

  def runZad(): Unit = {
    println("zad1")
    println(petlaFor())
    println(petlaForZP())
    println(petlaWhile())
  }

  def petlaFor(): String = {
    val output: StringBuilder = new StringBuilder
    days.foreach(f => output.append(s",$f"))
    output.deleteCharAt(0).toString()
  }

  def petlaForZP(): String = {
    val output: StringBuilder = new StringBuilder
    days.filter(f => f.startsWith("T")).foreach(f => output.append(s",$f"))
    output.deleteCharAt(0).toString()
  }

  def petlaWhile(): String = {
    var index = 1;
    val output: StringBuilder = new StringBuilder
    output.append(days.head)
    while (index < days.size) {
      output.append(s",${days(index)}")
      index += 1
    }
    output.toString()
  }
}
