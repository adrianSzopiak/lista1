object zad5 {

  val itemMap: Map[String, Double] = Map("phone" -> 1000, "mp3" -> 50, "earphones" -> 60, "toaster" -> 120)

  def runZad(): Unit = {
    println("zad5")
    println(itemMap.view.mapValues(value => value * 0.9).toMap)
  }

}
